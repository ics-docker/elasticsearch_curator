FROM python:3.6-alpine3.6
RUN pip install elasticsearch-curator==5.8.3
ENTRYPOINT ["/usr/local/bin/curator"]
